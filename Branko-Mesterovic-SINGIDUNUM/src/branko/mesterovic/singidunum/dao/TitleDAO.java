package branko.mesterovic.singidunum.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import branko.mesterovic.singidunum.entities.Title;

@Component
@Transactional
public class TitleDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Title> getAllTitles() {
		return session.getCurrentSession().createQuery("FROM Title").list();
	}

	public Title getTitle(int id) {
		return session.getCurrentSession().get(Title.class, id);
	}

	public Title insertTitle(Title title) {
		session.getCurrentSession().saveOrUpdate(title);
		return title;
	}
	
	public Title updateTitle(Title title) {
		return insertTitle(title);
	}

	public boolean deleteTitle(int id) {
		try {
			session.getCurrentSession().delete(getTitle(id));
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

}
