package branko.mesterovic.singidunum.dao;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import branko.mesterovic.singidunum.entities.Exam;

@Component
@Transactional
public class ExamDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Exam> getAllExams() {
		return session.getCurrentSession().createQuery("FROM Exam").list();
	}

	public Exam getExam(int id) {
		return session.getCurrentSession().get(Exam.class, id);
	}

	public Exam insertExam(Exam exam) {
		session.getCurrentSession().saveOrUpdate(exam);
		return exam;
	}
	
	public Exam updateExam(Exam exam) {
		return insertExam(exam);
	}

	public boolean deleteExam(int id) {
		try {
			session.getCurrentSession().delete(getExam(id));
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<Exam> getActiveExams() {
		Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
		return session.getCurrentSession().createQuery(
				"SELECT e FROM Exam e WHERE e.dateOfExam > :currentDate")
			    .setParameter("currentDate", currentDate)
			    .getResultList();
	}
	
}
