package branko.mesterovic.singidunum.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import branko.mesterovic.singidunum.entities.Exam;
import branko.mesterovic.singidunum.entities.Subject;

@Component
@Transactional
public class SubjectDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Subject> getAllSubjects() {
		return session.getCurrentSession().createQuery("FROM Subject").list();
	}

	public Subject getSubject(int id) {
		return session.getCurrentSession().get(Subject.class, id);
	}

	public Subject insertSubject(Subject subject) {
		session.getCurrentSession().saveOrUpdate(subject);
		return subject;
	}
	
	public Subject updateSubject(Subject subject) {
		return insertSubject(subject);
	}

	public boolean deleteSubject(int id) {
		try {
			for (Exam exam : getSubject(id).getExams()) {
				exam.setSubject(null);
			}
			session.getCurrentSession().delete(getSubject(id));
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
}
