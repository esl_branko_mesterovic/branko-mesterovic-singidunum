package branko.mesterovic.singidunum.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import branko.mesterovic.singidunum.entities.Student;

@Component
@Transactional
public class StudentDAO {

	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Student> getAllStudents() {
		return session.getCurrentSession().createQuery("FROM Student").list();
	}

	public Student getStudent(int id) {
		return session.getCurrentSession().get(Student.class, id);
	}

	public Student insertStudent(Student student) {
		session.getCurrentSession().saveOrUpdate(student);
		return student;
	}
	
	public Student updateStudent(Student student) {
		return insertStudent(student);
	}

	public boolean deleteStudent(int id) {
		try {
			session.getCurrentSession().delete(getStudent(id));
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

}
