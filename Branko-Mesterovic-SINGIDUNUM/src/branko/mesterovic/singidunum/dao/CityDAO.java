package branko.mesterovic.singidunum.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import branko.mesterovic.singidunum.entities.City;

@Component
@Transactional
public class CityDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<City> getAllCities() {
		return session.getCurrentSession().createQuery("FROM City").list();
	}

	public City getCity(int id) {
		return session.getCurrentSession().get(City.class, id);
	}

	public City insertCity(City city) {
		session.getCurrentSession().saveOrUpdate(city);
		return city;
	}
	
	public City updateCity(City city) {
		return insertCity(city);
	}

	public boolean deleteCity(int id) {
		try {
			session.getCurrentSession().delete(getCity(id));
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

}
