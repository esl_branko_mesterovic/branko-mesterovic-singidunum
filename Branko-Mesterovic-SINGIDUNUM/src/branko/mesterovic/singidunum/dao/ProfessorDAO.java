package branko.mesterovic.singidunum.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import branko.mesterovic.singidunum.entities.Exam;
import branko.mesterovic.singidunum.entities.Professor;

@Component
@Transactional
public class ProfessorDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Professor> getAllProfessors() {
		return session.getCurrentSession().createQuery("FROM Professor").list();
	}

	public Professor getProfessor(int id) {
		return session.getCurrentSession().get(Professor.class, id);
	}

	public Professor insertProfessor(Professor professor) {
		session.getCurrentSession().saveOrUpdate(professor);
		return professor;
	}
	
	public Professor updateProfessor(Professor professor) {
		return insertProfessor(professor);
	}

	public boolean deleteProfessor(int id) {
		try {
			for (Exam exam : getProfessor(id).getExams()) {
				exam.setProffesor(null);
			}
			session.getCurrentSession().delete(getProfessor(id));
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
}
