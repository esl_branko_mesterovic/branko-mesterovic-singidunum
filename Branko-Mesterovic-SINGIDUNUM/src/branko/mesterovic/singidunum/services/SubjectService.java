package branko.mesterovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import branko.mesterovic.singidunum.dao.SubjectDAO;
import branko.mesterovic.singidunum.entities.Subject;

@Service
public class SubjectService {
	
	@Autowired
	SubjectDAO subjectDao;
	
	public List<Subject> getAllSubjects() {
		return subjectDao.getAllSubjects();
	}

	public Subject getSubject(int id) {
		return subjectDao.getSubject(id);
	}

	public Subject insertSubject(Subject subject) {
		return subjectDao.insertSubject(subject);
	}
	
	public Subject updateSubject(Subject subject) {
		return subjectDao.updateSubject(subject);
	}

	public boolean deleteSubject(int id) {
		return subjectDao.deleteSubject(id);
	}
	
}
