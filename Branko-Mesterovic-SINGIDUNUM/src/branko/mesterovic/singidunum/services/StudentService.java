package branko.mesterovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import branko.mesterovic.singidunum.dao.StudentDAO;
import branko.mesterovic.singidunum.entities.Student;

@Service
public class StudentService {
	
	@Autowired
	StudentDAO studentDao;
	
	public List<Student> getAllStudents(){
		return studentDao.getAllStudents();
	}
	
	public Student getStudent(int id) {
		return studentDao.getStudent(id);
	}
		
	public Student insertStudent(Student student) {
		return studentDao.insertStudent(student);
	}
	
	public Student updateStudent(Student student) {
		return studentDao.updateStudent(student);
	}

	public boolean deleteStudent(int id) {
		return studentDao.deleteStudent(id);
	}
	
}
