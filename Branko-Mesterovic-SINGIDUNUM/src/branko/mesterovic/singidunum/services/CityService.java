package branko.mesterovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import branko.mesterovic.singidunum.dao.CityDAO;
import branko.mesterovic.singidunum.entities.City;

@Service
public class CityService {
	
	@Autowired
	CityDAO cityDao;
	
	public List<City> getAllCities() {
		return cityDao.getAllCities();
	}

	public City getCity(int id) {
		return cityDao.getCity(id);
	}

	public City insertCity(City city) {
		return cityDao.insertCity(city);
	}
	
	public City updateCity(City city) {
		return cityDao.updateCity(city);
	}

	public boolean deleteCity(int id) {
		return cityDao.deleteCity(id);
	}
	
}
