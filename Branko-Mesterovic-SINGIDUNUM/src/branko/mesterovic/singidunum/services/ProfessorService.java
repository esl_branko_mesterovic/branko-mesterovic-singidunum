package branko.mesterovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import branko.mesterovic.singidunum.dao.ProfessorDAO;
import branko.mesterovic.singidunum.entities.Professor;

@Service
public class ProfessorService {
	
	@Autowired
	ProfessorDAO professorDao;
	
	public List<Professor> getAllProfessors() {
		return professorDao.getAllProfessors();
	}

	public Professor getProfessor(int id) {
		return professorDao.getProfessor(id);
	}

	public Professor insertProfessor(Professor professor) {
		return professorDao.insertProfessor(professor);
	}
	
	public Professor updateProfessor(Professor professor) {
		return professorDao.updateProfessor(professor);
	}

	public boolean deleteProfessor(int id) {
		return professorDao.deleteProfessor(id);
	}

}
