package branko.mesterovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import branko.mesterovic.singidunum.dao.TitleDAO;
import branko.mesterovic.singidunum.entities.Title;

@Service
public class TitleService {
	
	@Autowired
	TitleDAO titleDao;
	
	public List<Title> getAllTitles() {
		return titleDao.getAllTitles();
	}

	public Title getTitle(int id) {
		return titleDao.getTitle(id);
	}

	public Title insertTitle(Title title) {
		return titleDao.insertTitle(title);
	}
	
	public Title updateTitle(Title title) {
		return titleDao.updateTitle(title);
	}

	public boolean deleteTitle(int id) {
		return titleDao.deleteTitle(id);
	}
	
}
