package branko.mesterovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import branko.mesterovic.singidunum.dao.ExamDAO;
import branko.mesterovic.singidunum.entities.Exam;

@Service
public class ExamService {
	
	@Autowired
	ExamDAO examDao;
	
	public List<Exam> getAllExams() {
		return examDao.getAllExams();
	}

	public Exam getExam(int id) {
		return examDao.getExam(id);
	}

	public Exam insertExam(Exam exam) {
		return examDao.insertExam(exam);
	}
	
	public Exam updateExam(Exam exam) {
		return examDao.updateExam(exam);
	}

	public boolean deleteExam(int id) {
		return examDao.deleteExam(id);
	}
	
	public List<Exam> getActiveExams() {
		return examDao.getActiveExams();
	}
	
}
