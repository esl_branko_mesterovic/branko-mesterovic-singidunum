package branko.mesterovic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import branko.mesterovic.singidunum.entities.Title;
import branko.mesterovic.singidunum.services.TitleService;

@Controller
public class TitleController {

	@Autowired
	private TitleService titleService;

	@RequestMapping("/titles")
	public String showTitles(Model model) {
		List<Title> titles = titleService.getAllTitles();
		System.out.println(titles);
		model.addAttribute("titles", titles);
		return "titles";
	}

	@RequestMapping("/titles/create")
	public String createTitle(Model model) {
		return "createtitle";
	}

	@RequestMapping(value = "/titles/add", method = RequestMethod.POST)
	public String doCreate(Model model, @Valid Title title, BindingResult result) {
		if (result.hasErrors()) {
			System.out.println("Form is not valid!");
			List<ObjectError> errors = result.getAllErrors();
			for (ObjectError e : errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		} 
		else {
			System.out.println("Form validated successfully!");
		}

		System.out.println(title);
		titleService.insertTitle(title);

		model.addAttribute("object", "Title");
		model.addAttribute("link", "titles");
		return "objectcreated";
	}

}
