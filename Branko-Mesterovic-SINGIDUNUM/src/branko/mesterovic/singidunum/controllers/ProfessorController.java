package branko.mesterovic.singidunum.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import branko.mesterovic.singidunum.entities.City;
import branko.mesterovic.singidunum.entities.Professor;
import branko.mesterovic.singidunum.entities.Title;
import branko.mesterovic.singidunum.services.CityService;
import branko.mesterovic.singidunum.services.ProfessorService;
import branko.mesterovic.singidunum.services.TitleService;

@Controller
public class ProfessorController {
	
	@Autowired
	private ProfessorService professorService;
	@Autowired
	private CityService cityService;
	@Autowired
	private TitleService titleService;
	
	@RequestMapping("/professors")
	public String showProfessors(Model model) {
		List<Professor> professors = professorService.getAllProfessors();
		model.addAttribute("professors", professors);
		return "professors";			
	}
	
	@RequestMapping("/professors/create")
	public String createProfessor(Model model) {
		List<City> cities = cityService.getAllCities();
		List<Title> titles = titleService.getAllTitles();
		System.out.println(cities);
		System.out.println(titles);
		model.addAttribute("cities", cities);
		model.addAttribute("titles", titles);
		return "createprofessor";
	}
	
	@RequestMapping(value = "/professors/add", method = RequestMethod.POST)
	public String doCreate(Model model, Professor professor, City city, Title title, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid!");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		}
		else {
			System.out.println("Form validated successfully!");
		}
		
		City professorCity = cityService.getCity(city.getCityId());
		Title professorTitle = titleService.getTitle(title.getTitleId());
		
		professor.setCity(professorCity);
		professor.setTitle(professorTitle);
		
		System.out.println(professor);
		
		professorService.insertProfessor(professor);
		
		model.addAttribute("object", "Professor");
		model.addAttribute("link", "professors");
		return "objectcreated";
	}
	
	@RequestMapping(value = "/professors/delete/{id}")
	public String deleteProfessor(@PathVariable int id, Model model) {
		professorService.deleteProfessor(id);
		model.addAttribute("object", "Professor");
		return "objectdeleted";
	}
	
	@RequestMapping(value ="/professors/edit/{id}")
	public String editProfessor(@PathVariable int id, Model model) {
		Professor professor = professorService.getProfessor(id);
		List<City> cities = cityService.getAllCities();
		List<Title> titles = titleService.getAllTitles();
		model.addAttribute("cities", cities);
		model.addAttribute("titles", titles);
		model.addAttribute("professor", professor);
		model.addAttribute("id", id);
		return "editprofessor";
	}
	
	@RequestMapping(value = "/professors/saveedit/{id}", method = RequestMethod.POST)
	public String editProfessorSave(@PathVariable int id, Model model, Professor professor, City city, Title title, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e: errors) {
				System.out.println(e.getDefaultMessage());
				model.addAttribute("message", "Something went wrong!");
				return "error";
			}
		}else {
			System.out.println("Form validated successsfully!");			
		}
		
		professor.setId(id);
		
		city = cityService.getCity(city.getCityId());
		professor.setCity(city);
		
		title = titleService.getTitle(title.getTitleId());
		professor.setTitle(title);
		
		professorService.updateProfessor(professor);
		
		model.addAttribute("object", "Professor");
		model.addAttribute("link", "professors");
		return "objectedited";
	}
	
}
