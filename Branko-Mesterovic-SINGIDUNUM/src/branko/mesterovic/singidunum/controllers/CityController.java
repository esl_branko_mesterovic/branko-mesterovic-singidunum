package branko.mesterovic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import branko.mesterovic.singidunum.entities.City;
import branko.mesterovic.singidunum.services.CityService;

@Controller
public class CityController {

	@Autowired
	private CityService cityService;

	@RequestMapping("/cities")
	public String showCities(Model model) {
		List<City> cities = cityService.getAllCities();
		System.out.println(cities);
		model.addAttribute("cities", cities);
		return "cities";
	}

	@RequestMapping("/cities/create")
	public String createCity(Model model) {
		return "createcity";
	}

	@RequestMapping(value = "/cities/add", method = RequestMethod.POST)
	public String doCreate(Model model, @Valid City city, BindingResult result) {
		if (result.hasErrors()) {
			System.out.println("Form is not valid!");
			List<ObjectError> errors = result.getAllErrors();
			for (ObjectError e : errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		} else {
			System.out.println("Form validated successfully!");
		}

		System.out.println(city);
		cityService.insertCity(city);

		model.addAttribute("object", "City");
		model.addAttribute("link", "cities");
		return "objectcreated";
	}

}
