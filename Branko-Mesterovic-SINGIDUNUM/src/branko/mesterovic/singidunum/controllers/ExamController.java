package branko.mesterovic.singidunum.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import branko.mesterovic.singidunum.entities.Exam;
import branko.mesterovic.singidunum.entities.Professor;
import branko.mesterovic.singidunum.entities.Student;
import branko.mesterovic.singidunum.entities.Subject;
import branko.mesterovic.singidunum.services.ExamService;
import branko.mesterovic.singidunum.services.ProfessorService;
import branko.mesterovic.singidunum.services.StudentService;
import branko.mesterovic.singidunum.services.SubjectService;

@Controller
public class ExamController {
	
	@Autowired
	private ExamService examService;
	@Autowired
	private SubjectService subjectService;
	@Autowired
	private ProfessorService professorService;
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/exams")
	public String showExams(Model model) {
		List<Exam> exams = examService.getAllExams();
		model.addAttribute("exams", exams);
		return "exams";
	}
	
	@RequestMapping("/exams/create")
	public String createExam(Model model) {
		List<Subject> subjects = subjectService.getAllSubjects();
		model.addAttribute("subjects", subjects);
		return "createexam";
	}
	
	@RequestMapping(value = "/exams/create/secondstep", method = RequestMethod.POST)
	public String assignSubjectStepTwo(Model model, Subject subject) {
		System.out.println("exam second step intro");
		subject = subjectService.getSubject(subject.getSubjectId());
		
//		System.out.println("sss " + subject);
		Set<Professor> professors = subject.getProfessors();
//		System.out.println(professors);
		
		model.addAttribute("professors", professors);
		model.addAttribute("subjectId", subject.getSubjectId());
		System.out.println("exam second step outro");
		return "createexamsecond";
	}
	
	@RequestMapping(value = "/exams/create/final", method = RequestMethod.POST)
	public String assignSubjectStepThree(Model model, Professor professor, Subject subject, Date dateOfExam) {
		System.out.println("exam final step intro");
//		System.out.println("Data check: prof: " + professor);
//		System.out.println("Data check: subj: " + subject);
		
		subject = subjectService.getSubject(subject.getSubjectId());
		professor = professorService.getProfessor(professor.getId());
		List<Exam> activeExams = examService.getActiveExams();
		System.out.println(activeExams.size());
		
		Exam exam = new Exam();
		exam.setSubject(subject);
		exam.setProffesor(professor);
		exam.setDateOfExam(dateOfExam);
		
		for(Exam e: activeExams) {
			if(e.getProffesor().getId() == exam.getProffesor().getId()
					&& e.getSubject().getSubjectId() == exam.getSubject().getSubjectId()) {
				System.out.println("Exam already exists!");
				model.addAttribute("message", "Exam already exists!");
				return "error";
			}
				
		}
		
		System.out.println("Upis ispita u bazu");
		examService.insertExam(exam);
		
		model.addAttribute("object", "Exam");
		model.addAttribute("link", "");
		System.out.println("exam final step intro");
		return "objectcreated";
	}
	
	@RequestMapping("/exams/registrate")
	public String registateExam(Model model) {
		List<Student> students = studentService.getAllStudents();
		model.addAttribute("students", students);
		return "registrateexam";
	}
	
	@RequestMapping(value = "/exams/registrate/secondstep", method = RequestMethod.POST)
	public String assignSubjectStepTwo(Model model, Student student) {
		student = studentService.getStudent(student.getId());
		
		Set<Subject> subjects = student.getSubjects();
		
		model.addAttribute("subjects", subjects);
		model.addAttribute("id", student.getId());
		
		return "registrateexamsecond";
	}
	
	@RequestMapping(value = "/exams/registrate/thirdstep", method = RequestMethod.POST)
	public String assignSubjectStepThree(Model model, Student student, Subject subject) {
		subject = subjectService.getSubject(subject.getSubjectId());
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 7);
		Date date = new Date(calendar.getTime().getTime());
		
		
		List<Exam> activeExams = examService.getActiveExams();
		
		for(Exam e: activeExams) {
			if(e.getSubject().getSubjectId() != subject.getSubjectId()
					&& e.getDateOfExam().after(date))
				activeExams.remove(e);
		}
		
		model.addAttribute("exams", activeExams);
		model.addAttribute("id", student.getId());
		
		return "registrateexamthird";
	}
	
	@RequestMapping(value = "/exams/registrate/final", method = RequestMethod.POST)
	public String assignSubjectStepFour(Model model, Student student, Exam exam) {
		student = studentService.getStudent(student.getId());
		exam = examService.getExam(exam.getExamId());
		Set<Student> students = exam.getStudents();
		students.add(student);
		exam.setStudents(students);
		examService.insertExam(exam);
		
		model.addAttribute("object", "Exam registration");
		model.addAttribute("link", "");
		return "objectcreated";
	}
	
}
