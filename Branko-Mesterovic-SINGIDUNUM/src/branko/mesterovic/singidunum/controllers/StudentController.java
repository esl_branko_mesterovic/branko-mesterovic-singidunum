package branko.mesterovic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import branko.mesterovic.singidunum.entities.City;
import branko.mesterovic.singidunum.entities.Professor;
import branko.mesterovic.singidunum.entities.Student;
import branko.mesterovic.singidunum.entities.Title;
import branko.mesterovic.singidunum.services.CityService;
import branko.mesterovic.singidunum.services.StudentService;

@Controller
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	@Autowired
	private CityService cityService;
	
	public String showStudentsHome() {
		return "studentshome";
	}
	
	@RequestMapping("/students")
	public String showStudents(Model model) {
		List<Student> students = studentService.getAllStudents();
		model.addAttribute("students", students);
		return "students";			
	}
	
	@RequestMapping("/students/create")
	public String createStudent(Model model) {
		List<City> cities = cityService.getAllCities();
		System.out.println(cities);
		model.addAttribute("cities", cities);
		return "createstudent";
	}
	
	@RequestMapping(value = "/students/add", method = RequestMethod.POST)
	public String doCreate(Model model, @Valid Student student, City city, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid!");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		}
		else {
			System.out.println("Form validated successfully!");
		}
		
		City studentCity = cityService.getCity(city.getCityId());
		student.setCity(studentCity);
		System.out.println(student);
		studentService.insertStudent(student);
		
		model.addAttribute("object", "Student");
		model.addAttribute("link", "students");
		return "objectcreated";
	}
	
	@RequestMapping(value = "/students/delete/{id}")
	public String deleteStudent(@PathVariable int id, Model model) {
		studentService.deleteStudent(id);
		model.addAttribute("object", "Student");
		return "objectdeleted";
	}
	
	@RequestMapping(value ="/students/edit/{id}")
	public String editStudent(@PathVariable int id, Model model) {
		Student student = studentService.getStudent(id);
		List<City> cities = cityService.getAllCities();
		model.addAttribute("cities", cities);
		model.addAttribute("student", student);
		model.addAttribute("id", id);
		return "editstudent";
	}
	
	@RequestMapping(value = "/students/saveedit/{id}", method = RequestMethod.POST)
	public String editStudentSave(@PathVariable int id, Model model, Student student, City city, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e: errors) {
				System.out.println(e.getDefaultMessage());
				model.addAttribute("message", "Something went wrong!");
				return "error";
			}
		}else {
			System.out.println("Form validated successsfully!");			
		}
		
		student.setId(id);
		
		city = cityService.getCity(city.getCityId());
		student.setCity(city);
		
		studentService.updateStudent(student);
		
		model.addAttribute("object", "Student");
		model.addAttribute("link", "students");
		return "objectedited";
	}
	
}
