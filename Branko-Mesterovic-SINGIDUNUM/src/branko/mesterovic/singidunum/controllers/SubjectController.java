package branko.mesterovic.singidunum.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import branko.mesterovic.singidunum.entities.City;
import branko.mesterovic.singidunum.entities.Exam;
import branko.mesterovic.singidunum.entities.Professor;
import branko.mesterovic.singidunum.entities.Student;
import branko.mesterovic.singidunum.entities.Subject;
import branko.mesterovic.singidunum.entities.Title;
import branko.mesterovic.singidunum.services.ProfessorService;
import branko.mesterovic.singidunum.services.StudentService;
import branko.mesterovic.singidunum.services.SubjectService;

@Controller
public class SubjectController {
	
	@Autowired
	private SubjectService subjectService;
	@Autowired
	private ProfessorService professorService;
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/subjects")
	public String showSubjects(Model model) {
		List<Subject> subjects = subjectService.getAllSubjects();
		model.addAttribute("subjects", subjects);
		return "subjects";			
	}
	
	@RequestMapping("/subjects/create")
	public String createSubject(Model model) {
		List<Professor> professors = professorService.getAllProfessors();
		model.addAttribute("professors", professors);
		return "createsubject";
	}
	
	@RequestMapping(value = "/subjects/add", method = RequestMethod.POST)
	public String doCreate(Model model, Subject subject, Professor professor, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid!");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
			}
			return "error";
		}
		else {
			System.out.println("Form validated successfully!");
		}
		
		System.out.println(professor);
		Set<Professor> professors = new HashSet<Professor>();
		
		professor = professorService.getProfessor(professor.getId());
		System.out.println(professor);
		
		professors.add(professor);
		subject.setProfessors(professors);
		System.out.println(subject);
		
		subjectService.insertSubject(subject);
		
		model.addAttribute("object", "Subject");
		model.addAttribute("link", "subjects");
		return "objectcreated";
	}
	
	@RequestMapping(value = "/subjects/assign")
	public String assignSubject(Model model) {
		List<Subject> subjects = subjectService.getAllSubjects();
		model.addAttribute("subjects", subjects);
		return "assignsubject";
	}
	
	@RequestMapping(value = "/subjects/assign/secondstep", method = RequestMethod.POST)
	public String assignSubjectStepOne(Model model, Subject subject) {
		subject = subjectService.getSubject(subject.getSubjectId());
		List<Professor> professors = professorService.getAllProfessors();
		for(Professor p: professors){
			if(p.getSubjects().contains(subject))
				professors.remove(p);
		}
		model.addAttribute("professors", professors);
		model.addAttribute("subjectId", subject.getSubjectId());
		return "assignsubjectsecond";
	}
	
	@RequestMapping(value = "/subjects/assign/final", method = RequestMethod.POST)
	public String assignSubjectStepTwo(Model model, Professor professor, Subject subject) {
		subject = subjectService.getSubject(subject.getSubjectId());
		professor = professorService.getProfessor(professor.getId());
		Set<Professor> professors = subject.getProfessors();
		professors.add(professor);
		subject.setProfessors(professors);
		subjectService.insertSubject(subject);
		
		model.addAttribute("object", "Subject assignment");
		model.addAttribute("link", "subjects");
		return "objectcreated";
	}
	
	@RequestMapping(value = "/subjects/assignto")
	public String assignToSubject(Model model) {
		List<Subject> subjects = subjectService.getAllSubjects();
		model.addAttribute("subjects", subjects);
		return "assigntosubject";
	}
	
	@RequestMapping(value = "/subjects/assignto/secondstep", method = RequestMethod.POST)
	public String assignToSubjectStepOne(Model model, Subject subject) {
		subject = subjectService.getSubject(subject.getSubjectId());
		List<Student> students = studentService.getAllStudents();
		for(Student s: students){
			if(s.getSubjects().contains(subject))
				students.remove(s);
		}
		System.out.println(students);
		model.addAttribute("students", students);
		model.addAttribute("subjectId", subject.getSubjectId());
		return "assigntosubjectsecond";
	}
	
	@RequestMapping(value = "/subjects/assignto/final", method = RequestMethod.POST)
	public String assignToSubjectStepTwo(Model model, Student student, Subject subject) {
		subject = subjectService.getSubject(subject.getSubjectId());
		student = studentService.getStudent(student.getId());
		Set<Subject> subjects = student.getSubjects();
		subjects.add(subject);
		student.setSubjects(subjects);
		studentService.insertStudent(student);
		
		model.addAttribute("object", "Subject assignment to student");
		model.addAttribute("link", "subjects");
		return "objectcreated";
	}
	
	@RequestMapping(value = "subjects/delete/{id}")
	public String deleteSubject(@PathVariable int id, Model model) {
		studentService.deleteStudent(id);
		model.addAttribute("object", "Subject");
		return "objectdeleted";
	}
	
	@RequestMapping(value ="/subjects/edit/{id}")
	public String editSubject(@PathVariable int id, Model model) {
		Subject subject = subjectService.getSubject(id);
		model.addAttribute("subject", subject);
		model.addAttribute("id", id);
		return "editsubject";
	}
	
	@RequestMapping(value = "/subjects/saveedit/{id}", method = RequestMethod.POST)
	public String editSubjectSave(@PathVariable int id, Model model, Subject subject, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e: errors) {
				System.out.println(e.getDefaultMessage());
				model.addAttribute("message", "Something went wrong!");
				return "error";
			}
		}else {
			System.out.println("Form validated successsfully!");			
		}
		
		subject.setSubjectId(id);
		
		Set<Professor> professors = subjectService.getSubject(id).getProfessors();
		subject.setProfessors(professors);
		
		Set<Exam> exams = subjectService.getSubject(id).getExams();
		subject.setExams(exams);
		
		subjectService.updateSubject(subject);
		
		model.addAttribute("object", "Professor");
		model.addAttribute("link", "professors");
		return "objectedited";
	}
	
}
