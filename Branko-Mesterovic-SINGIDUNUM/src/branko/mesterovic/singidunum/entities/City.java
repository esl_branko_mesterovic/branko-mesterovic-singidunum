package branko.mesterovic.singidunum.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "cities")
public class City {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "city_id")
	private int cityId;

	@NotNull
	@Size(min = 2, max = 30, message = "Name of city must be between 2 and 30 characters!")
	@Column(unique = true, name = "city_name")
	private String cityName;

	@OneToMany(mappedBy="city")
	private Set<Student> students;

	@OneToMany(mappedBy="city")
	private Set<Professor> professors;

	public City() {

	}

	public City(int cityId,
			@NotNull @Size(min = 2, max = 30, message = "Name of city must be between 2 and 30 characters!") String cityName,
			Set<Student> students, Set<Professor> professors) {
		super();
		this.cityId = cityId;
		this.cityName = cityName;
		this.students = students;
		this.professors = professors;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return "City [cityId=" + cityId + ", cityName=" + cityName + "]";
	}

}
