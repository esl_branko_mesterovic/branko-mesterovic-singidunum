package branko.mesterovic.singidunum.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.ForeignKey;
import javax.persistence.ConstraintMode;
import branko.mesterovic.singidunum.entities.Student;

@Entity
@Table(name = "exams")
public class Exam {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "exam_id")
	private int examId;

	@NotNull
	@Column(name = "date_od_exam")
	private Date dateOfExam;
	
	@ManyToOne
	private Subject subject;
	
	@ManyToOne
	private Professor professor;
	
	@ManyToMany(fetch = FetchType.LAZY,
	        cascade =
	        {
	                CascadeType.DETACH,
	                CascadeType.MERGE,
	                CascadeType.REFRESH,
	                CascadeType.PERSIST
	        },
	        targetEntity = Student.class)
	@JoinTable(name = "students_exams",
	        inverseJoinColumns = @JoinColumn(name = "student_id",
	                nullable = false,
	                updatable = false),
	        joinColumns = @JoinColumn(name = "exam_id",
	                nullable = false,
	                updatable = false),
	        foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
	        inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Student> students = new HashSet<Student>();
	

	public Exam(int examId, @NotNull Subject subject, @NotNull Professor professor, Set<Student> students,
			@NotNull Date dateOfExam) {
		super();
		this.examId = examId;
		this.subject = subject;
		this.professor = professor;
		this.students = students;
		this.dateOfExam = dateOfExam;
	}

	public Exam() {

	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Professor getProffesor() {
		return professor;
	}

	public void setProffesor(Professor professor) {
		this.professor = professor;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Date getDateOfExam() {
		return dateOfExam;
	}

	public void setDateOfExam(Date dateOfExam) {
		this.dateOfExam = dateOfExam;
	}

	@Override
	public String toString() {
		return "Exam [examId=" + examId + ", subject=" + subject + ", proffesor=" + professor + ", students=" + students
				+ ", dateOfExam=" + dateOfExam + "]";
	}

}
