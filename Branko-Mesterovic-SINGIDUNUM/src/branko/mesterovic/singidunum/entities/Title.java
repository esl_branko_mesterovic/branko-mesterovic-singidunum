package branko.mesterovic.singidunum.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "titles")
public class Title {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "title_id")
	private int titleId;

	@NotNull
	@Size(min = 2, max = 30, message = "Title must be between 2 and 30 characters!")
	@Column(unique = true, name = "title_name")
	private String titleName;

	@OneToMany(mappedBy="title")
	private Set<Professor> professors;

	public Title() {

	}

	public Title(int titleId,
			@NotNull @Size(min = 2, max = 30, message = "Title must be between 2 and 30 characters!") String titleName,
			Set<Professor> professors) {
		super();
		this.titleId = titleId;
		this.titleName = titleName;
		this.professors = professors;
	}

	public int getTitleId() {
		return titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public Set<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return "Title [titleId=" + titleId + ", titleName=" + titleName + "]";
	}

}
