package branko.mesterovic.singidunum.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.ForeignKey;
import javax.persistence.ConstraintMode;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "subjects")
public class Subject {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "subject_id")
	private int subjectId;

	@NotNull
	@Size(min = 3, message = "Subject name must be at least 3 and at most 30 characters!")
	private String name;

	@Size(min = 3, max = 200, message = "Description must be at least 3 and at most 200 characters!")
	private String description;

	@Range(min = 1, max = 4, message = "Please select only numbers from 1 to 4")
	@Column(name = "year_of_study")
	private int yearOfStudy;

	@Size(max = 10)
	private String semester;

	@ManyToMany(fetch = FetchType.LAZY,
	    cascade =
	    {
	            CascadeType.DETACH,
	            CascadeType.MERGE,
	            CascadeType.REFRESH,
	            CascadeType.PERSIST
	    },
	    targetEntity = Professor.class)
	@JoinTable(name = "professors_subjects",
	    inverseJoinColumns = @JoinColumn(name = "professor_id",
	            nullable = false,
	            updatable = false),
	    joinColumns = @JoinColumn(name = "subject_id",
	            nullable = false,
	            updatable = false),
	    foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
	    inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Professor> professors = new HashSet<Professor>();

	@ManyToMany(fetch = FetchType.LAZY,
	        cascade =
	        {
	                CascadeType.DETACH,
	                CascadeType.MERGE,
	                CascadeType.REFRESH,
	                CascadeType.PERSIST
	        },
	        targetEntity = Student.class)
	@JoinTable(name = "students_subjects",
	        inverseJoinColumns = @JoinColumn(name = "student_id",
	                nullable = false,
	                updatable = false),
	        joinColumns = @JoinColumn(name = "subject_id",
	                nullable = false,
	                updatable = false),
	        foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
	        inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Student> students = new HashSet<Student>();

	@OneToMany(mappedBy="subject")
	private Set<Exam> exams = new HashSet<Exam>();
	
	public Subject() {
		
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Set<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(Set<Professor> proffesors) {
		this.professors = proffesors;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	@Override
	public String toString() {
		return "Subject [subjectId=" + subjectId + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + ", professors=" + professors + ", students=" + students
				+ ", exams=" + exams + "]";
	}

}
