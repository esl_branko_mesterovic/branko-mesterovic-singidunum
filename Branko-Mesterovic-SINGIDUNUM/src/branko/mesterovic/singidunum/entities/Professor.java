package branko.mesterovic.singidunum.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.ForeignKey;
import javax.persistence.ConstraintMode;

@Entity
@Table(name = "professors")
public class Professor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "professor_id")
	private int id;

	@NotNull
	@Size(min = 3, max = 30, message = "First name must be between 3 and 30 characters!")
	@Column(name = "first_name")
	private String firstName;

	@NotNull
	@Size(min = 3, max = 30, message = "Last name must be between 3 and 30 characters!")
	@Column(name = "last_name")
	private String lastName;

	@Email(message = "Invalid email!")
	@Size(max = 30, message = "Email must have at most 30 characters!")
	private String email;

	@Size(min = 3, max = 50, message = "Adress must be between 3 and 50 characters!")
	private String adress;

	@ManyToOne
	private City city;

	@Size(min = 6, max = 15, message = "Phone must be between 5 and 15 character!")
	private String phone;

	@NotNull
	@Column(name = "reelecton_date")
	private Date reelectionDate;

	@ManyToOne
	private Title title;

	@OneToMany(mappedBy="professor")
	private Set<Exam> exams = new HashSet<Exam>();

	@ManyToMany(fetch = FetchType.LAZY,
	        cascade =
	        {
	                CascadeType.DETACH,
	                CascadeType.MERGE,
	                CascadeType.REFRESH,
	                CascadeType.PERSIST
	        },
	        targetEntity = Subject.class)
	@JoinTable(name = "professor_subjects",
	        joinColumns = @JoinColumn(name = "professor_id",
	                nullable = false,
	                updatable = false),
	        inverseJoinColumns = @JoinColumn(name = "subject_id",
	                nullable = false,
	                updatable = false),
	        foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
	        inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Subject> subjects = new HashSet<Subject>();

	public Professor() {

	}

	public Professor(int id,
			@NotNull @Size(min = 3, max = 30, message = "First name must be between 3 and 30 characters!") String firstName,
			@NotNull @Size(min = 3, max = 30, message = "Last name must be between 3 and 30 characters!") String lastName,
			@Email(message = "Invalid email!") @Size(max = 30, message = "Email must have at most 30 characters!") String email,
			@Size(min = 3, max = 50, message = "Adress must be between 3 and 50 characters!") String adress, City city,
			@Size(min = 6, max = 15, message = "Phone must be between 5 and 15 character!") String phone,
			@NotNull Date reelectionDate, @NotNull Title title, Set<Exam> exams, Set<Subject> subjects) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
		this.exams = exams;
		this.subjects = subjects;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String toString() {
		return "Professor [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", adress=" + adress + ", city=" + city + ", phone=" + phone + ", reelectionDate=" + reelectionDate
				+ ", title=" + title + "]";
	}

}
