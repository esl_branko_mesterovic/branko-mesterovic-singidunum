<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="ISO-8859-1">
	<title>Students</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body class="homebody">
    <div class="tablebuttonholder">
        <div class="tablebutton">
            <a href="${pageContext.request.contextPath}/">
                <i class="fas fa-arrow-left"></i>
            </a>
        </div>
        <div class="tablebutton">
            <a href="${pageContext.request.contextPath}/students/create">
                <i class="fas fa-plus"></i>
            </a>
        </div>
    </div>

	<table class="tableshow">
        <tr>
			<th>ID</th>
			<th>First name</th>
			<th>Last name</th>
			<th>Index</th>
			<th>City</th>
			<th>Address</th>
			<th>Year</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<c:forEach var="student" items="${students}">
			<tr>
				<td>
					<c:out value="${student.id}" />
				</td>
				<td>
					<c:out value="${student.firstName}" />
				</td>
				<td>
					<c:out value="${student.lastName}" />
				</td>
				<td>
					<c:out value="${student.indexNumber}" />
				</td>
				<td>
					<c:out value="${student.city.cityName}" />
				</td>
				<td>
					<c:out value="${student.adress}" />
				</td>
				<td>
					<c:out value="${student.currentYearOfStudy}" />
				</td>
				<td>
					<c:out value="${student.email}" />
				</td>
				<td>
					<c:out value="${student.phone}" />
				</td>
				<td>
					<a href="${pageContext.request.contextPath}/students/edit/${student.id}" class="edit"><i class="fas fa-edit"></i></a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath}/students/delete/${student.id}" onclick="return confirm('Are you sure?')" class="delete"><i class="fas fa-trash-alt"></i></a>
				</td>
			</tr>
		</c:forEach>
	</table>

</body>

</html>