<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create subject</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">


	<div class="content formcontent">
	<form action="${pageContext.request.contextPath}/subjects/add" method="post" id="subjectform">
		<label>Subject name: </label><input type="text" name="name" minlength="3" maxlength="30" required/> <br/>
		<label>Description: </label><textarea name="description" form="subjectform" maxlength=200></textarea> <br/>
		<label>Year of study: </label>
			<select name="yearOfStudy">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
			</select>
		<br/>
		<label>Semester: </label>
			<select name="semester">
				<option value="winter">Winter</option>
				<option value="summer">Summer</option>
			</select>
		
		<label>Select professor: </label> 
			<select name="id">
				<c:forEach var="professor" items="${professors}">
					<option value="${professor.id}">${professor.firstName} ${professor.lastName}</option>
				</c:forEach>
			</select>
		<br/>
		
		<input type="submit" value="Add subject"/>
	</form>
	</div>
	
</body>
</html>