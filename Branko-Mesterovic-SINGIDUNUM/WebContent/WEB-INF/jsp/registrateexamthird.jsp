<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Exams</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
<div class="content formcontent">
	<c:if test="${!empty exams}">
	    <form action="${pageContext.request.contextPath}/exams/registrate/final" method="post">
	    	<input name="id" value="${id}" type="hidden">
			<label>Select exam: </label>
				<select name="examId">
					<c:forEach var="exam" items="${exams}">
						<option value="${exam.examId}">${exam.examId} | ${exam.subject.name} ${exam.dateOfExam}</option>
					</c:forEach>
				</select>
			<br/>
			<input type="submit" value="Registrate"/>
		</form>
	</c:if>
	
	<c:if test="${empty exams}">		
		<p>No active exams for this subject.</p>
	    <p><a href="${pageContext.request.contextPath}/">Home</a></p>
	</c:if>
	</div>
</body>
</html>