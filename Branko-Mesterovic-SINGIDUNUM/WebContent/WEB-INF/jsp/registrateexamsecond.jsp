<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Subjects</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
<div class="content formcontent">
	<c:if test="${!empty subjects}">
	    <form action="${pageContext.request.contextPath}/exams/registrate/thirdstep" method="post">
	    	<input name="id" value="${id}" type="hidden">
			<label>Select subject: </label>
				<select name="subjectId">
					<c:forEach var="subject" items="${subjects}">
						<option value="${subject.subjectId}">${subject.subjectId} | ${subject.name}</option>
					</c:forEach>
				</select>
			<br/>
			<input type="submit" value="Next step"/>
		</form>
	</c:if>
	
	<c:if test="${empty subjects}">
		<p>Student has no subjects assigned.</p>
	    <p><a href="${pageContext.request.contextPath}/">Home</a></p>
	</c:if>
	</div>
</body>
</html>