<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Cities</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
	<div class="tablebuttonholder">
        <div class="tablebutton">
            <a href="${pageContext.request.contextPath}/">
                <i class="fas fa-arrow-left"></i>
            </a>
        </div>
        <div class="tablebutton">
            <a href="${pageContext.request.contextPath}/cities/create">
                <i class="fas fa-plus"></i>
            </a>
        </div>
    </div>

	<table class="tableshow">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    <c:forEach var="cityObject" items="${cities}">
	    <tr>
	        <td>
	            <c:out value="${cityObject.cityId}" />
	        </td>
	        <td>
	            <c:out value="${cityObject.cityName}" />
	        </td>
	    </tr>
    </c:forEach>
   	</table>
	
</body>
</html>