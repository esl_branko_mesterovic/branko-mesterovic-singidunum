<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<title>Professors</title>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
	<div class="tablebuttonholder">
        <div class="tablebutton">
            <a href="${pageContext.request.contextPath}/">
                <i class="fas fa-arrow-left"></i>
            </a>
        </div>
        <div class="tablebutton">
            <a href="${pageContext.request.contextPath}/professors/create">
                <i class="fas fa-plus"></i>
            </a>
        </div>
    </div>

	<table class="tableshow">
    <tr>
        <th>ID</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Title</th>
        <th>City</th>
        <th>Address</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Re-election date</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="professor" items="${professors}">
	    <tr>
	        <td>
	            <c:out value="${professor.id}" />
	        </td>
	        <td>
	            <c:out value="${professor.firstName}" />
	        </td>
	        <td>
	            <c:out value="${professor.lastName}" />
	        </td>
	        <td>
	            <c:out value="${professor.title.titleName}" />
	        </td>
	        <td>
	            <c:out value="${professor.city.cityName}" />
	        </td>
	        <td>
	            <c:out value="${professor.adress}" />
	        </td>
	        <td>
	            <c:out value="${professor.email}" />
	        </td>
	        <td>
	            <c:out value="${professor.phone}" />
	        </td>
	        <td>
	            <c:out value="${professor.reelectionDate}" />
	        </td>
	        <td>
	        	<a href="${pageContext.request.contextPath}/professors/edit/${professor.id}" class="edit"><i class="fas fa-edit"></i></a>
	        </td>
	        <td>
	        	<a href="${pageContext.request.contextPath}/professors/delete/${professor.id}" onclick="return confirm('Are you sure?')" class="delete"><i class="fas fa-trash-alt"></i></a>
	        </td>
	    </tr>
    </c:forEach>
   	</table>
	
</body>
</html>