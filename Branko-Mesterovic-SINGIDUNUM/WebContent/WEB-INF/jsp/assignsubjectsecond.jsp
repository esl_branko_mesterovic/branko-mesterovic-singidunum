<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Subjects</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
<div class="content formcontent">
	<c:if test="${!empty professors}">
		<form action="${pageContext.request.contextPath}/subjects/assign/final" method="post" id="subjectform">
			<input name="subjectId" value="${subjectId}" type="hidden">
			<label>Select professor: </label>
				<select name="id">
					<c:forEach var="professor" items="${professors}">
						<option value="${professor.id}">${professor.id} | ${professor.title.titleName} ${professor.firstName} ${professor.lastName}</option>
					</c:forEach>
				</select>
			<br/>
			<input type="submit" value="Assign"/>
		</form>
	</c:if>
	
	<c:if test="${empty professors}">		
		<p>No professors to assign subject to.</p>
	    <p><a href="${pageContext.request.contextPath}/">Home</a></p>
	</c:if>
	</div>
</body>
</html>