<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Professors - Home</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
    
    <div class="content content2">
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/exams/create">
            <i class="fas fa-calendar-plus"></i>
                <h2>Enschedule exam</h2>
            </a>

        </div>
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/professors">
            <i class="fas fa-database"></i>
                <h2>Professors data</h2>
            </a>
        </div>
    </div>
    
</body>
</html>