<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Subjects</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
<div class="content formcontent">
	<c:if test="${!empty students}">
		<form action="${pageContext.request.contextPath}/subjects/assignto/final" method="post" id="subjectform">
			<input name="subjectId" value="${subjectId}" type="hidden">
			<label>Select student: </label>
				<select name="id">
					<c:forEach var="student" items="${students}">
						<option value="${student.id}">${student.id} | ${student.indexNumber} | ${student.firstName} ${student.lastName}</option>
					</c:forEach>
				</select>
			<br/>
			<input type="submit" value="Assign"/>
		</form>
	</c:if>
	
	<c:if test="${empty students}">		
		<p>No students to add.</p>
	    <p><a href="${pageContext.request.contextPath}/">Home</a></p>
	</c:if>
	</div>
</body>
</html>