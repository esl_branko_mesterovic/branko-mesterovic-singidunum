<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create professor</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
	<div class="content formcontent">
	<form action="${pageContext.request.contextPath}/professors/saveedit/${id}" method="post">
		<label>First name: </label><input type="text" name="firstName" minlength="3" maxlength="30" value="${professor.firstName}" required/> <br/>
		<label>Last name: </label><input type="text" name="lastName" minlength="3" maxlength="30" value="${professor.lastName}" required/> <br/>
		<label>Email: </label><input type="email" name="email" maxlength="30" value="${professor.email}"/> <br/>
		<label>Phone: </label><input type="text" name="phone" pattern="[0-9]{6,15}" value="${professor.phone}"/> <br/>
		<label>Address: </label><input type="text" name="adress" minlength="3" maxlength="50" value="${professor.adress}"/> <br/>
		<label>Title: </label>
			<select name="titleId">
				<c:forEach var="title" items="${titles}">
					<option value="${title.titleId}">${title.titleName}</option>
				</c:forEach>
			</select>
		<br/>
		<label>City: </label>
			<select name="cityId">
				<c:forEach var="city" items="${cities}">
					<option value="${city.cityId}">${city.cityName}</option>
				</c:forEach>
			</select>
		 <br/>
 		 <label>Re-election date: </label><input type="date" id="reelectionDate" name="reelectionDate" value="${professor.reelectionDate}" required />
 		 <br/>
		
		<input type="submit" value="Save edit"/>
	</form>
	
	<p><a href="${pageContext.request.contextPath}/">Back</a></p>
	</div>
	<script type="text/javascript">
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		 if(dd<10){
		        dd='0'+dd
		    } 
		    if(mm<10){
		        mm='0'+mm
		    }
	
		today = yyyy+'-'+mm+'-'+dd;
		document.getElementById("reelectionDate").setAttribute("min", today);
	</script>
</body>
</html>