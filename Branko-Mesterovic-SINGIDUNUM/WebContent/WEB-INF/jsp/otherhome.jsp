<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Other</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
    
    <div class="content content2">
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/cities">
            <i class="fas fa-map-marked-alt"></i>
                <h2>Cities</h2>
            </a>

        </div>
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/titles">
            <i class="fas fa-id-card"></i>
                <h2>Titles</h2>
            </a>
        </div>
    </div>
    
</body>
</html>