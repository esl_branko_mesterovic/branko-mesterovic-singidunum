<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create student</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
	<div class="content formcontent">
	<form action="${pageContext.request.contextPath}/students/add" method="post">
		<label>First name: </label><input type="text" name="firstName" minlength="3" maxlength="30" required/> <br/>
		<label>Last name: </label><input type="text" name="lastName" minlength="2" maxlength="30" required/> <br/>
		<label>Email: </label><input type="email" name="email"/> <br/>
		<label>Phone: </label><input type="text" name="phone" pattern="[0-9]{6,15}"/> <br/>
		<label>Adress: </label><input type="text" name="adress" minlength="3" maxlength="50"/> <br/>
		<label>Current year of study: </label>
			<input type="number" name="currentYearOfStudy" min="1" max="5" required>
		<br/>
		<label>Index: </label><input type="text" name="indexNumber" minlength="10" maxlength="10" required/> <br/>
		<label>City: </label> 
			<select name="cityId">
				<c:forEach var="city" items="${cities}">
					<option value="${city.cityId}">${city.cityName}</option>
				</c:forEach>
			</select>
		 <br>
		
		<input type="submit" value="Add student"/>
	</form>
	</div>
</body>
</html>