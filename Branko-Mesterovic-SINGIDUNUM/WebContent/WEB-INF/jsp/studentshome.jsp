<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Students - Home</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
    
    <div class="content content3">
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/exams/registrate">
            <i class="fas fa-sticky-note"></i>
                <h2>Exam registration</h2>
            </a>

        </div>
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/students">
            <i class="fas fa-database"></i>
                <h2>Students data</h2>
            </a>
        </div>
        <div class="contentbox">
            <a href="${pageContext.request.contextPath}/subjects/assignto">
            <i class="fas fa-user-plus"></i>
                <h2>Assign to subject</h2>
            </a>
        </div>
    </div>
    
</body>
</html>