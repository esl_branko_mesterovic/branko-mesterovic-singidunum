<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create city</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/all.css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="homebody">
	<div class="content formcontent">
	<form action="${pageContext.request.contextPath}/cities/add" method="post">
		<label>City name: </label><input type="text" name="cityName" minlength="2" maxlength="30" required/> <br/>
		
		<input type="submit" value="Add city"/>
	</form>
	</div>
	
</body>
</html>